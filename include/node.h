#pragma once

class Node
{
public:
    Node(double x = 0, double y = 0, double z = 0, double w = 0);
    ~Node();

    friend const Node operator+(const Node &first, const Node &second);
    friend const Node operator-(const Node &first, const Node &second);
    Node& operator=(const Node &node);
    friend const Node operator*(const Node &node, double value);
    friend const Node operator/(const Node &node, double value);

public:
    double x, y, z, w;
private:

};


