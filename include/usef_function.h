#pragma once
#include "node.h"
#include <vector>
#include <functional>

void rotateXW(Node &node, double angle)
{
    double y, z;
    y = node.y * cos(angle) - node.z * sin(angle);
    z = node.y * sin(angle) + node.z * cos(angle);
    node.y = y;
    node.z = z;
}

void rotateYW(Node &node, double angle)
{
    double x, z;
    x =  node.x * cos(angle) + node.z * sin(angle);
    z = -node.x * sin(angle) + node.z * cos(angle);
    node.x = x;
    node.z = z;
}

void rotateZW(Node &node, double angle)
{
    double x, y;
    x = node.x * cos(angle) - node.y * sin(angle);
    y = node.x * sin(angle) + node.y * cos(angle);
    node.x = x;
    node.y = y;
}

void rotateXY(Node &node, double angle)
{
    double z, w;
    z = node.z * cos(angle) - node.w * sin(angle);
    w = node.z * sin(angle) + node.w * cos(angle);
    node.z = z;
    node.w = w;
}

void rotateXZ(Node &node, double angle)
{
    double y, w;
    y = node.y * cos(angle) - node.w * sin(angle);
    w = node.y * sin(angle) + node.w * cos(angle);
    node.y = y;
    node.w = w;
}
void rotateYZ(Node &node, double angle)
{
    double w, x;
    w = node.w * cos(angle) - node.x * sin(angle);
    x = node.w * sin(angle) + node.x * cos(angle);
    node.x = x;
    node.w = w;
}

void moveToW(Node &node, double offset)
{
    node.w += offset;
}

void moveToX(Node &node, double offset)
{
    node.x += offset;
}

void moveToY(Node &node, double offset)
{
    node.y += offset;
}
void moveToZ(Node &node, double offset)
{
    node.z += offset;
}

void scale(Node &node, double scale)
{
    node = node * scale;
}

std::function<void(Node&, double)> f_rotateXW = rotateXW;
std::function<void(Node&, double)> f_rotateYW = rotateYW;
std::function<void(Node&, double)> f_rotateZW = rotateZW;

std::function<void(Node&, double)> f_rotateXY = rotateXY;
std::function<void(Node&, double)> f_rotateXZ = rotateXZ;
std::function<void(Node&, double)> f_rotateYZ = rotateYZ;

std::function<void(Node&, double)> f_moveToW  = moveToW;
std::function<void(Node&, double)> f_moveToX  = moveToX;
std::function<void(Node&, double)> f_moveToY  = moveToY;
std::function<void(Node&, double)> f_moveToZ  = moveToZ;

std::function<void(Node&, double)> f_scale    = scale;


