#pragma once

#include <vector>
#include "edge.h"
#include "node.h"

class Face
{
public:
    Face();
    ~Face();
    Face(std::vector<Edge> edges);

    void addEdge(const Edge &edge);
    std::vector<Edge>& getEdges();

private:
    std::vector<Edge> _edges;

};

