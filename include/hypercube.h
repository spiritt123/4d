#pragma once

#include <vector>
#include "face.h"
#include "edge.h"
#include "node.h"
#include <functional>

class Hypercube
{
public:
    Hypercube();
    Hypercube(std::vector<Face> faces);
    ~Hypercube();
    
    std::vector<Face> getFaces();
    std::vector<Node> getNodes();
    std::vector<Edge> getEdge();
    void applyUserFunction(std::function<void (Node&, double)> user_function, double vatiable);

private:
    std::vector<Face> _faces;
};

