#pragma once

#include "node.h"

class Edge
{
public:
    Edge();
    ~Edge();
    Edge(const Node &start, const Node &end);
    Edge& operator=(const Edge &edge);

    Node& getStartNode();
    Node& getEndNode();
    const Node& getStartNode() const;
    const Node& getEndNode() const;
    void setStartNode(const Node &node);
    void setEndNode(const Node &node);

private:
    Node _start;
    Node _end;
};

