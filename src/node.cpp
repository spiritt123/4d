#include "node.h" 

Node::~Node()
{
}

Node::Node(double x, double y, double z, double w)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = w;
}

const Node operator+(const Node &first, const Node &second)
{
    return {
        first.x + second.x,
        first.y + second.y,
        first.z + second.z,
        first.w + second.w
    };
}

const Node operator-(const Node &first, const Node &second)
{
    return {
        first.x - second.x,
        first.y - second.y,
        first.z - second.z,
        first.w - second.w
    };
}

Node& Node::operator=(const Node &node)
{
    if (this == &node)
    {
        return *this;
    }

    this->x = node.x;
    this->y = node.y;
    this->z = node.z;
    this->w = node.w;

    return *this;
}

const Node operator*(const Node &node, double value)
{
    return {
        node.x * value,
        node.y * value,
        node.z * value,
        node.w * value
    };
}

const Node operator/(const Node &node, double value)
{
    return {
        node.x / value,
        node.y / value,
        node.z / value,
        node.w / value
    };
}

