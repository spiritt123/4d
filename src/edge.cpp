#include "edge.h"
#include "node.h"

Edge::Edge()
{
    _start = Node();
    _end = Node();
}

Edge::~Edge()
{
}

Edge::Edge(const Node &start, const Node &end)
{
    _start = start;
    _end   = end;
}

Edge& Edge::operator=(const Edge &edge)
{
    if (this == &edge)
    {
        return *this;
    }

    _start = edge.getStartNode();
    _end   = edge.getEndNode();

    return *this;
}

Node& Edge::getStartNode() 
{
    return _start;
}

Node& Edge::getEndNode() 
{
    return _end;
}

const Node& Edge::getStartNode() const
{
    return _start;
}

const Node& Edge::getEndNode() const
{
    return _end;
}

void Edge::setStartNode(const Node &node)
{
    _start = node;
}

void Edge::setEndNode(const Node &node)
{
    _end = node;
}



