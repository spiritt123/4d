#include "face.h"

Face::Face()
{
}

Face::~Face()
{
}

Face::Face(std::vector<Edge> edges)
{
    _edges.resize(edges.size());
    std::copy(edges.begin(), edges.end(), _edges.begin());
}

void Face::addEdge(const Edge &edge)
{
    _edges.push_back(edge);
}

std::vector<Edge>& Face::getEdges()
{
    return _edges;
}



