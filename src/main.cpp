#include <iostream>

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <functional>

#include <SFML/Graphics.hpp>

//#include "rotate.h"
#include "usef_function.h"
#include "node.h"
#include "edge.h"
#include "hypercube.h"

double getDistance(const Node &start, const Node &end)
{
    double x = start.x - end.x;
    double y = start.y - end.y;
    double z = start.z - end.z;
    double w = start.w - end.w;
    return sqrt(x*x + y*y + z*z + w*w);
}

double findDotForLine(double start, double end, double answer, double e)
{
    if (abs(start - end) < e)
    {
        return start;
    }
    else
    {
        return answer * (end - start) + start;
    }
}

std::vector<Edge> sliceFace(Face face)
{
    double e = 0.0001;
    std::vector<Node> line;
    std::vector<Edge> edges;
    for (const auto &edge : face.getEdges())
    {
        double x, y, z, answer;
        Node start = edge.getStartNode();
        Node end   = edge.getEndNode();
        if (start.w * end.w > 0)
        {
            continue;
        }
        if (abs(start.w - end.w) < e)
        {
            if (abs(start.w) > e)
            {
                continue;
            }
            std::cout << ">>>" << start.w << "\n\n";
            edges.push_back(edge);
            std::cout << edges.size();
            continue;
        }
        answer = start.w / (end.w - start.w);
        
        x = findDotForLine(start.x, end.x, answer, e);
        y = findDotForLine(start.y, end.y, answer, e);
        z = findDotForLine(start.z, end.z, answer, e);

        Node center(x, y, z, 0);

        if (abs(getDistance(start, end) - getDistance(start, center) - getDistance(end, center)) > e)
        {
            continue;
        }
        line.push_back(center);
    }
    if (line.size() > 2)
    {
        edges.push_back({line[0], line[1]});
    }
    return edges;
}

void show(sf::RenderWindow &window, std::vector<Edge> edges)
{
    double scale = 70.f;
    sf::Vector2f move(300, 300);

    for (const auto edge : edges)
    {
        sf::Vector2f start(edge.getStartNode().x * scale + move.x, 
                           edge.getStartNode().y * scale + move.y);
        sf::Vector2f end  (edge.getEndNode().x * scale + move.x, 
                           edge.getEndNode().y * scale + move.y);
        
        //std::cout << start.x << " : " << start.y << "\n";
        //std::cout << end.x << " : " << end.y << "\n";
        sf::Vertex line[] =
        {
            sf::Vertex(start),
            sf::Vertex(end),
        
        };
        window.draw(line, 2, sf::Lines);
    }
}

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML works!");
    window.setFramerateLimit(60);

    //Hypercube cube;

    //cube.applyUserFunction(f_scale   , 3);

//    cube.applyUserFunction(f_moveToW , -0.5);
    //cube.applyUserFunction(f_moveToX , 1.2);
    //cube.applyUserFunction(f_moveToY , 1.2);
    //cube.applyUserFunction(f_moveToZ , 1.2);
    
    
    //cube.applyUserFunction(f_rotateXY, 0.78);
    //cube.applyUserFunction(f_rotateXZ, 0.78);
    //cube.applyUserFunction(f_rotateYZ, 0.78);

    double angle = 0.005;
    int i = 0;
    
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Escape) window.close();             
            }
        }
        
        //window.clear();
        

        if (++i > 200)
        {
            angle *= -1;
            i = 0;
        }

        //cube.applyUserFunction(f_rotateXW, 0.01);
        //cube.applyUserFunction(f_rotateYW, 0.01);
        //cube.applyUserFunction(f_rotateZW, 0.01);
        //cube.applyUserFunction(f_rotateXY, 0.01);
        
        //cube.applyUserFunction(f_moveToW , angle);
        
        /*
        std::vector<Edge> edges;
        for (auto face : cube.getFaces())
        {
            std::vector<Edge> slice_edges= sliceFace(face);
            if (slice_edges.size() != 0)
            {
                //std::cout << slice_edges.size();
                edges.insert(edges.end(), slice_edges.begin(), slice_edges.end());
            }
        }
        */
        //show(window, edges);
        //show(window, cube.getEdge());
        //window.display();
    }

    return 0;
}




